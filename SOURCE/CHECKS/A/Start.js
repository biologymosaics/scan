

(async () => {
	await require ("./../../index.js") ({
		DIRECTORY: __dirname,
		EXTENSIONS: [ "js" ],

		CONFIG: {
			"env": {
				"browser": true,
				"es2021": true,
				"node": true
			},
			"extends": "eslint:recommended",
			"parserOptions": {
				"ecmaVersion": 12
			},
			"rules": {
				"no-mixed-spaces-and-tabs": "off",

				"no-unused-vars": "warn"
			}
		},

		// this is called one line before the async return statement
		CONCLUDE: ({ MESSAGE }) => {
			console.log (MESSAGE);
		},

		UNSUCCESSFUL: ({ THROW }) => {
			console.error ("SCAN", { THROW });
		}
	});
}) ();
