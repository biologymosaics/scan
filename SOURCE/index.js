
/*
	NOTES:
	./node_modules/eslint/bin/eslint.js . --ext .js -c .eslintrc.js
	
*/

const { ESLint } = require ("eslint");

var PERFORMABLE = require ("path").resolve (
	__dirname,
	"node_modules/eslint/bin/eslint.js"
);

module.exports = async function ({
	DIRECTORY,
	EXTENSIONS = [ "js" ],
	CONFIG,
	CONCLUDE,
	UNSUCCESSFUL
}) {
	var MESSAGE;

	try {
		const LINT = new ESLint ({
			overrideConfig: CONFIG
		});

		const FORMATTER = await LINT.loadFormatter ("stylish");

		const LOCATIONS = EXTENSIONS.map (EXTENSION => {
			return require ("path").resolve (DIRECTORY, `**/*.${ EXTENSION }`);
		});

		// console.log ({
		// 	LOCATIONS
		// });

		// 2. Lint files.
		const Results = await LINT.lintFiles (LOCATIONS);

		/*
			Make the location paths relative to the directory
		*/
		Results.map (Result => {
			Result.filePath = require ("path").relative (
				DIRECTORY,
				Result.filePath
			);

			return Result
		});

		MESSAGE = FORMATTER.format (Results);
	}
	catch (THROW) {
		UNSUCCESSFUL ({ THROW });
		return;
	}

	CONCLUDE ({ MESSAGE });
	return;
}
