
const CWD = __dirname;

const {
    ESLint
} = require ("eslint");

(async function main() {
    // 1. Create an instance.
    const LINT = new ESLint ();
	const FORMATTER = await LINT.loadFormatter ("stylish");

    // 2. Lint files.
    const Results = await LINT.lintFiles ([
		"Check/**/*.js"
	]);

	// console.dir ({ Results }, {
	// 	depth: 10
	// });
	Results.map (Result => {
		Result.filePath = require ("path").relative (
			CWD,
			Result.filePath
		);

		return Result
	});

    // 3. Format the results.
    const MESSAGE = FORMATTER.format (Results);

    // 4. Output it.
    console.log (MESSAGE);
})().catch((error) => {
    process.exitCode = 1;
    console.error(error);
});
