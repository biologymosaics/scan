
/*
	var { Code } = await require ("./AUX/Story") ({
		Label: `[ Story ]`,
		Script: `pm2 start "${ Open }" --name "${ Name }"`,
		ENV: Object.assign ({}, process.env, {
			NODE_PATH: Modules
		})
	});
*/


function Parse (Data, Conclude) {
	Data.toString ().split ("\n").filter (Line => {
		return Line.trim ().length >= 1;
	})
	.forEach (Line => {
		Conclude (Line);
	});
}

module.exports = function ({
	Script,
	ENV = false,
	Mode = "exec",
	CWD = process.cwd (),
	Label = "[]",
	Messages = null
}) {
	return new Promise (Conclude => {
		// console.log ("STORY", {
		// 	Script
		// });

		var Proc = require ("child_process") [ Mode ] (
			Script, {
				cwd: CWD,
				encoding: "utf8",
				shell: "/bin/sh",
				...(Mode === "fork" ? {
					stdio: [ 'pipe', 'pipe', 'pipe', 'ipc' ]
				} : {
					stdio: [ 'pipe', 'pipe', 'pipe' ]
				}),
				...(ENV ? { env: ENV } : {})
			}
		);

		Proc.stdout.on ("data", (DATA) => {
			Parse (DATA, (Line) => {
				var Message = [
				   require('chalk').cyan (Label),
				   " ",
				   Line,
				   "\n"
			   ].join ("");

			   if (typeof Messages === "function") {
				   Messages ({
					   Message
				   });
			   }
			   else {
				   process.stdout.write (Message);
			   }
			});
		});

		Proc.stderr.on ('data', (DATA) => {
			Parse (DATA, (Line) => {
				 var Message = [
 				   require('chalk').bgRed (require('chalk').white (Label)),
 				   " ",
 				   Line,
 				   "\n"
 			   ].join ("");

				if (typeof Messages === "function") {
 				   Messages ({
 					   Message
 				   });
 			   }
 			   else {
 				   process.stderr.write (Message);
 			   }
			});
		});

		Proc.on ('close', (Code) => {
			var Message = [
			   require('chalk').blue (Label),
			   " Closed with code: ",
			   Code,
			   "\n"
			].join ("")

			if (typeof Messages === "function") {
			   Messages ({
				   Message
			   });
			}
			else {
			   process.stdout.write (Message);
			}

			Conclude ({ Code });
		});

		Proc.on ('exit', (Code) => {
			// console.log ('exit');
		});
		Proc.on ('error', (Code) => {
			console.log ('error');
		});
		Proc.on ('message', (Code) => {
			console.log ('message');
		});

		// console.log (Proc.pid);
	});
}
